<?php

/**
 *
 * Copyright (c) 2011 SoftChalk LLC - https://softchalk.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      Michael Moses <michael@softchalk.com>
 * @copyright   2011 SoftChalk LLC
 * @license     http://www.gnu.org/licenses/
 * @link        https://softchalk.com
 *
 */


require_once($CFG->dirroot . '/mod/softchalk/lib.php');

class softchalk_lesson {
    public $id;
    public $course_id;
    public $dataroot;
    public $lessonrevisionnumber;
    public $points;
    public $name;
    public $lessonbuilderid;
    public $manifest_file;
    
    public function __construct($id, $course_id) {
        $this->id = $id;
        $this->course_id = $course_id;
        $this->dataroot = softchalk_get_dataroot();
        $this->set_manifest_file();
    }
    
    private function set_vars($vars) {
        $this->lessonrevisionnumber = $vars['lessonrevisionnumber'][0];
        $this->points = $vars['points'][0];
        $this->name = $vars['name'][0];
        $this->lessonbuilderid = $vars['id'][0];
    }

    public function get_lesson_by_id($id){
        return get_record('softchalk', 'id', $id);
    }
    
    public function get_lesson_directory() {
        return $this->dataroot . '/' . $this->course_id . '/moddata/softchalk/' . $this->id;
    }
    
    public function get_manifest_name($with_leading_slash=true) {
        $slash = '/';

        if($with_leading_slash == false) {
            $slash = '';
        }
        return $slash . 'softchalk_manifest.xml';
    }
    
    public function get_manifest_file() {
        return $this->get_lesson_directory($this->dataroot, $this->course_id, $this->id) . $this->get_manifest_name();
    }
    
    private function set_manifest_file() {
        $this->manifest_file = $this->get_manifest_file();
    }
    
    public function parse_manifest() {
        $m = new manifest($this->manifest_file);
        $this->set_vars($m->parse());
    }
}

class manifest {
    public $filename;
    
    public function __construct($filename) {
        $this->filename = $filename;
    }
    
    public function parse() {
        $xml = new SimpleXmlIterator($this->filename, null, true);
        $namespaces = $xml->getNamespaces(true);
        return $this->xmlToArray($xml,$namespaces);
    }
    
    public function xmlToArray($xml,$ns=null) {
        $a = array();
        for ($xml->rewind(); $xml->valid(); $xml->next()) {
            $key = $xml->key();

            if (!isset($a[$key])) { $a[$key] = array(); $i=0; }
            else $i = count($a[$key]);

            $simple = true;
            foreach($xml->current()->attributes() as $k=>$v) {
                $a[$key][$i][$k]=(string)$v;
                $simple = false;
            }
            if ($ns) foreach($ns as $nid=>$name) {
                foreach($xml->current()->attributes($name) as $k=>$v) {
                    $a[$key][$i][$nid.':'.$k]=(string)$v;
                    $simple = false;
                }
            } 

            if ($xml->hasChildren()) {
                if($simple) $a[$key][$i] = $this->xmlToArray($xml->current(), $ns);
                else $a[$key][$i]['content'] = $this->xmlToArray($xml->current(), $ns);
            }
            else {
                if($simple) $a[$key][$i] = strval($xml->current());
                else $a[$key][$i]['content'] = strval($xml->current());
            }
            $i++;
        }
        return $a;
    }
}

class softchalk_import {
    public function __construct() {
        global $CFG;
        require_once($CFG->dirroot . '/mod/softchalk/lib.php');
        $this->root = softchalk_get_dataroot();
        $this->softchalk_root = softchalk_get_softchalk_root();
    }
    
    public function scorecenter() {
        require_once($this->softchalk_root .'/scorecenter/lib.php');
    }
    
    public function locallib() {
        require_once($this->softchalk_root .'/locallib.php');
    }
    
    public function oauth() {
        require_once($this->softchalk_root . '/security/oauth_test.php');  
    } 
    
    public function softchalk_lib(){
        require_once($this->softchalk_root . '/lib.php');
    }
    
    public function json(){
        require_once($this->softchalk_root . '/upgradephp-17/upgrade.php');
    }
}

class lti{
    public $resource_link_id;
    public $context_id;
    public $user_id;
    public $custom_aggregation_model;
    public $tool_consumer_instance_guid;
    public $lti_version;
    public $custom_max_completed_attempts;
    public $custom_max_uncompleted_attempts;
    public $ext_ims_lis_simple_outcome_url;
    public $custom_attempt_url;
    public $custom_manifest_xml;
    public $custom_cms;

    public function __construct(){
        $this->custom_cms = 'moodle1.9';
        $this->lti_version = 'blti';
    }
}

?>
