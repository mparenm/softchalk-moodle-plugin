<?php

/**
 *
 * Copyright (c) 2011 SoftChalk LLC - https://softchalk.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      Michael Moses <michael@softchalk.com>
 * @copyright   2011 SoftChalk LLC
 * @license     http://www.gnu.org/licenses/
 * @link        https://softchalk.com
 *
 */


$CFG->softchalk_root = $CFG->dirroot . '/mod/softchalk';

// messages
$softchalk_response = Array(
    'success' => false,
    'message' => ''
    );

require_once(softchalk_get_softchalk_root() . '/softchalk.php');
$softchalk_import = new softchalk_import();


/**
 * SoftChalk Module install function
 */
function softchalk_install() {
    global $CFG;
    
    // create module record
    $module = new stdClass;
    $module->name = 'SoftChalk';
    insert_record('modules', $module);

    // create report permission capabilites
    $role = new stdClass;
    $role->context_id = 1;
    $role->id = 1;
    $role->capability = 'gradereport/'.$module->name.':view';
    $role->permission = 1;
    $role->timemodified = time();
    $role->modifierid = 0;
    insert_record('role_capabilities', $role);
    
    $role->id = 3;
    insert_record('role_capabilities', $role);
    
    $role->id = 4;
    insert_record('role_capabilities', $role);
    
    // copy lang file into the main lang area
    if(!file_exists(softchalk_get_webroot() . '/lang/en_utf8/softchalk.php')) {
        copy(softchalk_get_softchalk_root() . '/lang/en_utf8/softchalk.php',
             softchalk_get_webroot() . '/lang/en_utf8/softchalk.php');
    }
}

/**
 * Creates a softchalk scorecenter record
 *
 * @return id of the new record
 */
function softchalk_add_instance($softchalk){
    global $CFG, $USER;
    
    require_once($CFG->dirroot.'/mod/softchalk/softchalk.php');

    $import = new softchalk_import();
    $import->locallib();
    
    if (($packagedata = softchalk_check_package($softchalk)) != null){
        $softchalk->pkgtype = $packagedata->pkgtype;
        $softchalk->datadir = $packagedata->datadir;
        
        if (!softchalk_external_link($softchalk->filename)){
            $softchalk->md5hash = md5_file($CFG->dataroot.'/'.$softchalk->course.'/'.$softchalk->filename);
        }
        else{
            $softchalk->dir = $CFG->dataroot.'/'.$softchalk->course.'/moddata/softchalk';
            $softchalk->md5hash = md5_file($softchalk->dir.$softchalk->datadir.'/'.basename($softchalk->filename));
        }
        
        if (!isset($softchalk->whatgrade)){
            $softchalk->whatgrade = 0;
        }
        
        $softchalk->filename = $softchalk->filename;
        $softchalk->timemodified = time();
        $softchalk->created = time();
        $softchalk->updated = time();
        
        $id = insert_record('softchalk', $softchalk);
        $softchalk->id = $id;
        
        if (softchalk_external_link($softchalk->filename) || ((basename($softchalk->filename) != 'imsmanifest.xml') && ($softchalk->filename[0] != '#'))) {
            $softchalk->dir = $CFG->dataroot.'/'.$softchalk->course.'/moddata/softchalk';
            rename($softchalk->dir.$softchalk->datadir,$softchalk->dir.'/'.$id) or die('could not rename');

        }
        
        // parse softchalk manifest and assign the point value
        $scl = new softchalk_lesson($softchalk->id, $softchalk->course);
        $scl->parse_manifest();
        
        $softchalk->maxgrade = $scl->points;
        
        softchalk_grade_item_update(stripslashes_recursive($softchalk));
        
        // add scorecenter item
        require_once($CFG->softchalk_root . "/scorecenter/lib.php");
        $scorecenter = new scorecenter();
        $scorecenter->set_lms_data($softchalk->course, $softchalk->id, $USER->id);
        $scorecenter->custom_aggregation_model = $softchalk->whatgrade;
        $scorecenter->maxattempt = $softchalk->maxattempt;
        $scorecenter->create($softchalk->id);

        return $id;
    }
    else {
        print_error('badpackage','softchalk');
    }
} 

/**
 * Returns the updated record
 *
 * @return an updated instance record
 */
function softchalk_update_instance($softchalk){
    global $CFG, $USER;

    require_once($CFG->dirroot.'/mod/softchalk/softchalk.php');
    $import = new softchalk_import();
    $import->locallib();
    
    $softchalk->filename = $softchalk->filename;
    $softchalk->updated = time();
    $softchalk->timemodified = time();

    $softchalk->id = $softchalk->instance;
    
    return update_record('softchalk', $softchalk);
}

function softchalk_delete_instance($softchalk){
}

function softchalk_user_outline(){
}

function softchalk_user_complete(){
}

function softchalk_get_view_actions(){
    return array(
        "view",
        "view all",
        "report"
    );
}

function softchalk_get_post_actions(){
    return array(
        "attempt",
        "editquestions",
        "review",
        "submit"
    );
}

/**
 * Returns a softchalk grades object
 *
 * @return softchalk grades object
 */
function softchalk_get_user_grades($softchalk, $userid=0) {
    global $CFG;
    require_once($CFG->dirroot.'/mod/softchalk/locallib.php');

    $grades = array();

    if (empty($userid)) {
        if ($scousers = get_records_select("softchalk_scoes_track", "softchalkid='$softchalk->id' GROUP BY userid", "", "userid,null")) {
            foreach ($scousers as $scouser) {
                $grades[$scouser->userid] = new object();
                $grades[$scouser->userid]->id = $scouser->userid;
                $grades[$scouser->userid]->userid = $scouser->userid;
                $grades[$scouser->userid]->rawgrade = softchalk_grade_user($softchalk, $scouser->userid);
            }
        }
        else {
            return false;
        }

    }
    else {
        if (!get_records_select("softchalk_scoes_track", "softchalkid='$softchalk->id' AND userid='$userid' GROUP BY userid", "", "userid, null")) {
            //no attempt yet
            return false;
        }
        $grades[$userid] = new object();
        $grades[$userid]->id = $userid;
        $grades[$userid]->userid = $userid;
        $grades[$userid]->rawgrade = softchalk_grade_user($softchalk, $userid);
    }

    return $grades;
}

/**
 * Update grades in central gradebook
 *
 * @param object $softchalk null means all softchalkbases
 * @param int $userid specific user only, 0 mean all
 */
function softchalk_update_grades($softchalk=null, $userid=0, $nullifnone=true) {
    global $CFG;
    if (!function_exists('grade_update')) {
        // workaround for some PHP versions
        require_once($CFG->libdir.'/gradelib.php');
    }

    if ($softchalk != null) {
        if ($grades = softchalk_get_user_grades($softchalk, $userid)) {
            softchalk_grade_item_update($softchalk, $grades);

        }
        elseif ($userid and $nullifnone) {
            $grade = new object();
            $grade->userid   = $userid;
            $grade->rawgrade = NULL;
            softchalk_grade_item_update($softchalk, $grade);

        }
        else {
            softchalk_grade_item_update($softchalk);
        }

    }
    else {
        $sql = "select
                    s.*,
                    cm.idnumber as cmidnumber
                from
                    {$CFG->prefix}softchalk s,
                    {$CFG->prefix}course_modules cm,
                    {$CFG->prefix}modules m
                where
                    m.name='softchalk'
                    and m.id=cm.module
                    and cm.instance=s.id";
        if ($rs = get_recordset_sql($sql)) {
            while ($softchalk = rs_fetch_next_record($rs)) {
                softchalk_update_grades($softchalk, 0, false);
            }
            rs_close($rs);
        }
    }
}

/**
 * Update/create grade item for given softchalk
 *
 * @param object $softchalk object with extra cmidnumber
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return object grade_item
 */
function softchalk_grade_item_update($softchalk, $grades=NULL) {
    global $CFG;

    if (!function_exists('grade_update')) {
        require_once($CFG->libdir.'/gradelib.php');
    }

    $params = array('itemname'=>$softchalk->name);
    if (isset($softchalk->cmidnumber)) {
        $params['idnumber'] = $softchalk->cmidnumber;
    }

    /*
    if (($softchalk->grademethod % 10) == 0) { // GRADESCOES
        if ($maxgrade = count_records_select('softchalk_scoes',"softchalk='$softchalk->id' AND launch<>'".sql_empty()."'")) {
            $params['gradetype'] = GRADE_TYPE_VALUE;
            $params['grademax']  = $maxgrade;
            $params['grademin']  = 0;
        } else {
            $params['gradetype'] = GRADE_TYPE_NONE;
        }
    } else {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax']  = $softchalk->maxgrade;
        $params['grademin']  = 0;
    }
    */

    if ($grades  === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }

    return grade_update('mod/softchalk', $softchalk->course, 'mod', 'softchalk', $softchalk->id, 0, $grades, $params);
}

/**
 * Delete grade item for given softchalk
 *
 * @param object $softchalk object
 * @return object grade_item
 */
function softchalk_grade_item_delete($softchalk) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    return grade_update(
        'mod/softchalk', $softchalk->course, 'mod',
        'softchalk', $softchalk->id, 0, NULL,
        array('deleted'=>1));
}

/**
 * Returns an array of the view actions
 *
 * @return array array of view actions
 */
function _softchalk_get_view_actions() {
    return array(
        'pre-view',
        'view',
        'view all',
        'report'
    );
}

function _softchalk_get_post_actions() {
    return array();
}

/**
 * Returns an associative array of the update options
 *
 * @return array array of update options
 */
function _softchalk_get_update_options(){
    return array(
        "never" => 0,
        "onchange" => 1,
        "everyday" => 2,
        "everytime" => 3
    );
}

/**
 * Returns an associative array of the grade options
 *
 * @return array array of grade options
 */
function _softchalk_get_grade_options(){
    return array(
        "scoes" => 0,
        "highest" => 1,
        "average" => 2,
        "sum" => 3,
    );
}

/**
 * Returns an associative array of the attempt options
 *
 * @return array array of attempt options
 */
function _softchalk_get_attempt_options(){
    return array(
        "highest" => 0,
        "average" => 1,
        "first" => 2,
        "last" => 3
    );
}

function softchalk_is_moddata_trusted() {
    return true;
}

/**
 * Returns the softchalk scorecenter domain
 *
 * @param bool $link
 *
 * @return string scorecenter domain URL or href with domain
 */
function softchalk_get_scorecenter_domain($link=false){
    if($link == false) {
        return 'https://www.softchalkconnect.com';
    }
    return "<a href='https://www.softchalkconnect.com'>https://www.softchalkconnect.com</a>";
}

/**
 * Returns the softchalkconnect URL for moodle plugin registration
 *
 * @param bool $link
 *
 * @return string registration domain URL,  or href with domain
 */
function softchalk_get_moodle_registration_url($link=false){
    if($link == false) {
        return 'https://www.softchalkconnect.com/services/registration/moodle';
    }
    return "<a href='https://www.softchalkconnect.com/services/registration/moodle'>https://www.softchalkconnect.com/services/registration/moodle</a>";
}

/**
 * Returns config object for softchalk
 *
 * @return config object
 */
function softchalk_get_admin_settings() {
    return get_records_select("config", "name like "softchalk_%"", "id ASC");
}

/**
 * Returns a JSON encoded version of the passed dictionary
 *
 * @return a JSON encoded version of the passed dictionary
 */
function softchalk_build_response($response) {
    return json_encode($response); 
}

function softchalk_get_dataroot() {
    global $CFG;
    return $CFG->dataroot;
}

function softchalk_get_webroot() {
    global $CFG;
    return $CFG->dirroot;
}

function softchalk_get_softchalk_root() {
    global $CFG;
    return $CFG->softchalk_root;
}

/**
 * Iterate and print elements of an array
 *
 * @param array $array
 * @exit bool stop all execution with exit() call
 * returns nothing
 */
function _hojo($array, $exit=true){
    foreach($array as $k => $v){
        if(empty($v)) {
            $v = 'EMPTY';
        }
        echo '<strong>'.$k.'</strong>'.'<br />'.(string)$v.'<br />';
    }

    if($exit == true){
        exit();
    }
}

function softchalk_modify_nav($nav){
    /* drop the element for SoftChalk link */
    if($nav['navlinks']){
        $b = explode('<li', $nav['navlinks']);
        unset($b[3]);
        $nav['navlinks'] = implode('<li', $b);
    }
    return $nav;
}

function softchalk_get_scorecenter_test_page(){
    $test_page = '../mod/softchalk/scorecenter/test_page.php';
    
    return ("<div style='height:100px; width: 600px;'>"
           . "<iframe src='" . $test_page . "' style='width:100%; height:100%;' frameborder='0'>"
           . "</iframe>"
           . "</div>");
}

?>
