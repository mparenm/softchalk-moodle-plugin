<?php

/**
 *
 * Copyright (c) 2011 SoftChalk LLC - https://softchalk.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      Michael Moses <michael@softchalk.com>
 * @copyright   2011 SoftChalk LLC
 * @license     http://www.gnu.org/licenses/
 * @link        https://softchalk.com
 *
 */


class scorecenter {
    public $id;
    public $enabled;
    public $moodle_url;
    public $scorecenter_url;
    public $oauth_key;
    public $oauth_secret;
    public $manifest_xml;
    public $course_id;
    public $lesson_id;
    public $user_id;
    public $custom_aggregation_model;
    public $post_data;
    public $maxattempt;
    public $user_list;
    
    public function __construct() {
        $settings = softchalk_get_admin_settings();
        $this->settings = Array();

        /* admin page bug with scorecenter URL, need to check if that value exists */
        
        foreach($settings as $k => $v) {
            $name = str_replace('softchalk_', '', $settings[$k]->name);
            $this->settings[$name] = $settings[$k]->value;
        }
        
        $this->enabled = $this->settings['enabled'];
        $this->get_course_module_id();
        $this->moodle_url = $this->settings['moodle_url'];
        $this->scorecenter_url = $this->settings['scorecenter_url'];
        $this->oauth_key = $this->settings['oauth_key'];
        $this->oauth_secret = $this->settings['oauth_secret'];
        $this->oauth_test_url = $this->settings['scorecenter_url'] . "/api/scorecenter/test/oauth";
        $this->content_properties_url = $this->scorecenter_url . '/api/scorecenter/content/new';
        $this->content_attempt_details_url = $this->scorecenter_url . '/scorecenter/gradebook/html';
        $this->api_url = $this->moodle_url . '/mod/softchalk/scorecenter/';
    }
    
    public function is_enabled() {
        return $this->enabled;
    }
    
    public function create($content_id) {
        global $CFG;
        $this->get_xml_manifest();
        $this->post_data = $this->build_post_data();
        
        require_once($CFG->dirroot.'/mod/softchalk/softchalk.php');
        
        #softchalk_includes
        $import = new softchalk_import();
        $import->oauth();
        
        $scor = new SoftChalkOauthRequest();
        $scor->build($this->oauth_key, $this->oauth_secret, $this->content_properties_url, $this->post_data);
    }
    
    public function verify() {
        global $CFG;
        
        $this->post_data = $this->build_post_data();
        
        $this->post_data['resource_link_id'] = $this->lesson_id;
        
        require_once($CFG->dirroot . '/mod/softchalk/security/oauth_test.php');  
	    
        $response = init($this->oauth_key, $this->oauth_secret, $this->scorecenter_url . '/api/scorecenter/content', $this->post_data);
        echo $response;
    }
    
    public function get_course_module_id() {
        $this->cmid = 0;
        
        if(isset($_GET['id'])) {
            $this->cmid = $_GET['id'];
        }
    }
        
    public function build_api_url($action) {
        $api_url = '?a=' . $action;
        return $this->api_url . $api_url;
    }
    
    public function get_xml_manifest() {
        $this->manifest_xml = $this->set_xml_manifest();
    }
    
    public function set_xml_manifest() {
        global $CFG;
        
        $file_path = $CFG->dataroot . '/' . $this->course_id . '/' . $CFG->moddata . '/softchalk/' . $this->lesson_id;
        $f = fopen($file_path . '/softchalk_manifest.xml', 'rb') or die("Unable to parse SoftChalk XML Manifest<br />File Path: " . $file_path . '/softchalk_manifest.xml');
        $c = stream_get_contents($f);
        fclose($f);
        return urlencode($c);
    }
    
    public function set_cookie($content_url) {
        $now = time();
        $name = "scorecenter_" . $now;
        $data = $this->build_cookie_data($content_url);
        setcookie($name, $data, $now + (60 * 1), '/');
    }
    
    protected function build_cookie_data($content_url) {
        return "user_id=" . rawurlencode($this->user_id) . "|" . 
            "context_id=" . rawurlencode($this->course_id) . "|" . 
            "resource_link_id=" . rawurlencode($this->lesson_id) . "|" .
            "tool_consumer_instance_guid=" . rawurlencode($this->institution_id) . "|" .
            "custom_resource_link_url=" . rawurlencode($content_url) . "|" .
            "custom_scorecenter_host=" . rawurlencode($this->scorecenter_url);
    }
    
    private function set_course_id($course) {
        $this->course_id = $course;
    }
    
    private function set_lesson_id($lesson) {
        $this->lesson_id = $lesson;
    }
    
    private function set_user_id($user)
    {
        $this->user_id = $user;
    }
    
    public function set_lms_data($course_id, $lesson_id, $user_id) {
        $this->institution_id = $this->oauth_key;
        $this->set_course_id($course_id);
        $this->set_lesson_id($lesson_id);
        $this->set_user_id($user_id);
    }
    
    public function test_oauth(){
        global $CFG;
        
        # will need to check for curl lib before enabling scorecenter in admin settings
        if(extension_loaded('curl')) {
            require_once($CFG->softchalk_root . '/security/oauth_test.php');
        }
        
        require_once($CFG->softchalk_root . '/security/oauth_test.php');
        $scor = new SoftChalkOauthRequest();
        
        if(!function_exists('json_decode')){
            $import = new softchalk_import();
            $import->json();
        }
        return json_decode($scor->test($this->oauth_key, $this->oauth_secret, $this->oauth_test_url), true);
    }

    public function details(){
        global $CFG, $USER;
        require_once($CFG->dirroot.'/mod/softchalk/softchalk.php');
        
        #softchalk_includes
        $import = new softchalk_import();
        $import->oauth();
        
        $this->post_data = array(
            'resource_link_id' => $this->lesson_id,
            'context_id' => $this->course_id,
            'user_id' => $this->user_id,
            'custom_user_map' => $this->getUserList($this->lesson_id, $this->course_id),
            'roles' => 'instructor',
        );

        $scor = new SoftChalkOauthRequest();
        return $scor->build($this->oauth_key, $this->oauth_secret, $this->content_attempt_details_url, $this->post_data, $send=false);
    }
    
    public function build_post_data(){
        $post['resource_link_id'] = $this->lesson_id;
        $post['context_id'] = $this->course_id;
        $post['user_id'] = $this->user_id;
        $post['custom_aggregation_model'] = $this->custom_aggregation_model;
        $post['tool_consumer_instance_guid'] = $this->oauth_key;
        $post['lti_version'] = 'blti';
        $post['custom_max_completed_attempts'] = $this->maxattempt;
        $post['custom_max_uncompleted_attempts'] = $this->maxattempt;
        $post['ext_ims_lis_simple_outcome_url'] = $this->build_api_url('updategrade');
        $post['custom_attempt_url'] = $this->build_api_url('getattempt');
        $post['custom_manifest_xml'] = $this->manifest_xml;
        $post['custom_cms'] = 'moodle1.9';
        $post['resource_link_title'] = '';
        $post['custom_encoded_resource_link_title'] = urlencode('');
        return $post;
    }
    
    public function getAttemptUrl(){
        return softchalk_get_scorecenter_domain() . '/scorecenter/content/attempt/list';
    }
    
    public function getAttemptUrlLink($linkText){
        return "<a href='" . $this->getAttemptUrl() . "'>" . $linkText . "</a>";
    }

    public function getUserList($id, $course){
        /* need to deal with json issue */
        if(!function_exists('json_encode')){
            require_once(softchalk_get_softchalk_root() . '/upgradephp-17/upgrade.php');
            $import = new softchalk_import();
            $import->json();
        }

        return json_encode($this->buildUserArray($this->getUserListById($this->getUserIdsFromGrades($id, $course))));
    }

    public function buildUserArray($user_list){
        $users = array();
        $x=0;
        
        if($user_list != false){
            foreach($user_list as $k => $v){
                $users[$x]['id'] = $user_list[$k]->id;
                $users[$x]['username'] = $user_list[$k]->username;
                $users[$x]['firstname'] = $user_list[$k]->firstname;
                $users[$x]['lastname'] = $user_list[$k]->lastname;
                $x += 1;
            }
        }
        return $users;
    }

    public function getUserListById($ids){
        /*
            feel the array with a 0 if empty
            to avoid an empty value for the 
            get_records_list set of values 
            since it uses an IN() to search by.
        */
        
        if(count($ids) == 0){
            $ids = array('0');
        }
        return get_records_list($table='user', $field='id', $values=implode(',', $ids), $fields='id, username, firstname, lastname');
    }

    public function getUserIdsFromGrades($id, $course){
        if($id){
            $item=get_record('grade_items', 'iteminstance', $id, 'courseid', $course, 'itemmodule', 'softchalk');
        }
        else{
            $item=get_records('grade_items', 'courseid', $course);
        }
        
        $ids = Array();
        
        if($item != false){
            if(!$id){$item = $this->removeNonSoftchalkRecords($item);}
            foreach($item as $k => $v){
                $grades=get_records('grade_grades', 'itemid', $v); 
                    if ($grades != false){
                        foreach($grades as $k => $v){
                            array_push($ids, $v->userid);
                        }
                    }
                }
        }
        return array_unique($ids);
    }
    
    public function removeNonSoftchalkRecords($r){
        $new_set = array();
        foreach($r as $k => $v){
            if($v->itemmodule == 'softchalk'){
                array_push($new_set, $k);
            }
        }
        return $new_set;
    }
}

?>
