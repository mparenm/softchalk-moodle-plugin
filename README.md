SoftChalk Moodle Plugin
===
---

These are code samples from the SoftChalk Moodle Plugin.

This plugin was written to add LTI support to Moodle 1.9, and is **no longer
supported**, as Moodle now has integrated LTI support.

Visit https://softchalk.com for more information on their products and
services.


# License
---

Licensed under the GNU GPL License <http://www.gnu.org/copyleft/gpl.html>.
